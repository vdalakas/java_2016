/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simpleclock;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Random;

/**
 *
 * @author vdalakas
 */
public class SimpleClock {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Simple contructor
        Clock myClock = new Clock();
        // Set values for instance myClock
        myClock.setFormat24(false);
        myClock.setHour(1);
        myClock.setMin(27);
 
        //note a single Random object is reused here
        Random rand = new Random();
        for (int i = 0; i < 10; i++) {
            myClock.createAlarm(rand.nextInt(24), rand.nextInt(60));
        }
        myClock.listAlarms();

//        myClock.setHour_alarm(10);
//        myClock.setMin_alarm(30);
        myClock.setName("Rolex");
        // Display time
        myClock.tellTime();
        // Advance time
        myClock.advanceTime(1320);
        // Display time to check result of advanceTime()
        myClock.tellTime();
        // This is something asked in the exercise of Lab2
        System.out.println(myClock.getName().contains("o"));
        // This is something asked in the exercise of Lab2
        Clock myClock1 = new Clock(1, 27);
        //Clock myClock2 = new Clock(1, 27, 10, 30);
        // Check clock instances
        myClock1.howManyClocks();
        myClock1 = null;
        // Clock.howManyClocks();

        // Call to Garbage Collector
        System.gc();

        try {
            Thread.sleep(5000);
        } catch (InterruptedException ex) {
            Logger.getLogger(SimpleClock.class.getName()).log(Level.SEVERE, null, ex);
        }

        // Check clock instances
        myClock.howManyClocks();
        // Clock.howManyClocks();
    }

}
