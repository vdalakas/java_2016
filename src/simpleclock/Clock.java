/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simpleclock;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author vdalakas
 */
public class Clock {

    private NumberDisplay clockHours;
    private NumberDisplay clockMinutes;
//    private NumberDisplay alarmHours;
//    private NumberDisplay alarmMinutes;
    private ArrayList<Alarm> alarms = new ArrayList<Alarm>();

    private boolean format24;
    private String name;
    public static int clocks;

    public Clock() {
        this.clockHours = new NumberDisplay();
        this.clockHours.setLimit(24);
        this.clockHours.setValue(0);

        this.clockMinutes = new NumberDisplay();
        this.clockMinutes.setLimit(60);
        this.clockMinutes.setValue(0);
        /*
         this.alarmHours = new NumberDisplay();
         this.alarmHours.setLimit(24);
         this.alarmHours.setValue(0);

         this.alarmMinutes = new NumberDisplay();
         this.alarmMinutes.setLimit(60);
         this.alarmMinutes.setValue(0);
         */
        this.name = "";
        this.format24 = false;
        this.clocks++;
    }

    public Clock(int currentHour, int currentMinutes) {
        this.clockHours = new NumberDisplay();
        this.clockHours.setLimit(24);
        this.clockHours.setValue(currentHour);

        this.clockMinutes = new NumberDisplay();
        this.clockMinutes.setLimit(60);
        this.clockMinutes.setValue(currentMinutes);
        /*
         this.alarmHours = new NumberDisplay();
         this.alarmHours.setLimit(24);
         this.alarmHours.setValue(0);

         this.alarmMinutes = new NumberDisplay();
         this.alarmMinutes.setLimit(60);
         this.alarmMinutes.setValue(0);
         */
        this.name = "";
        this.format24 = false;
        this.clocks++;
    }

    public Clock(int currentHour, int currentMinutes, int alarmHours, int alarmMinutes) {
        this.clockHours = new NumberDisplay();
        this.clockHours.setLimit(24);
        this.clockHours.setValue(currentHour);

        this.clockMinutes = new NumberDisplay();
        this.clockMinutes.setLimit(60);
        this.clockMinutes.setValue(currentMinutes);
        /*
         this.alarmHours = new NumberDisplay();
         this.alarmHours.setLimit(24);
         this.alarmHours.setValue(alarmHours);

         this.alarmMinutes = new NumberDisplay();
         this.alarmMinutes.setLimit(60);
         this.alarmMinutes.setValue(alarmMinutes);
         */
        name = "";
        format24 = false;
        clocks++;
    }

    @Override
    protected void finalize() {
        try {
            clocks--;
        } finally {
            try {
                super.finalize();
            } catch (Throwable ex) {
                Logger.getLogger(Clock.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void howManyClocks() {
        System.out.println("Current clock number is " + clocks);
    }

    /*    
     //  You may define the method as static void instead of void since it contains only one static field
     public static void howManyClocks() {
     System.out.println("Current clock number is " + clocks);
     }
     */
    int getHour() {
        return clockHours.getValue();
    }

    int getMin() {
        return clockMinutes.getValue();
    }
    /*
     int getHour_alarm() {
     return alarmHours.getValue();
     }

     int getMin_alarm() {
     return alarmMinutes.getValue();
     }
     */

    boolean getFormat24() {
        return format24;
    }

    String getName() {
        return name;
    }

    void setHour(int a) {
        clockHours.setValue(a);
    }

    void setMin(int a) {
        clockMinutes.setValue(a);
    }
    /*
     void setHour_alarm(int a) {
     alarmHours.setValue(a);
     }

     void setMin_alarm(int a) {
     alarmMinutes.setValue(a);
     }
     */

    void setName(String a) {
        name = a;
    }

    void setFormat24(boolean a) {
        format24 = a;
    }

    void advanceTime(int advanceMinutes) {
        int tHour = 0; // To store temporary hours
        tHour = clockMinutes.advanceValueWithOverflowCount(advanceMinutes);
        clockMinutes.advanceValue(advanceMinutes);
        clockHours.advanceValue(tHour);
    }

    /*
     Added String.format( "%02d", fieldName ) in order to format 
     fieldName during println. %02d uses 2 digits and prints 01 instead of 1 
     improving the overall appearance.
     */
    void tellTime() {
        if (format24) {
            System.out.println("The time now is: " + String.format("%02d", clockHours.getValue()) + ":" + String.format("%02d", clockMinutes.getValue()));
        } else {
            if (clockHours.getValue() == 0) {
                System.out.println("The time now is: " + "12" + ":" + String.format("%02d", clockMinutes.getValue()) + "am");
            } else if (clockHours.getValue() > 0 && clockHours.getValue() < 12) {
                System.out.println("The time now is: " + String.format("%02d", clockHours.getValue()) + ":" + String.format("%02d", clockMinutes.getValue()) + "am");
            } else if (clockHours.getValue() == 12) {
                System.out.println("The time now is: " + "12" + ":" + String.format("%02d", clockMinutes.getValue()) + "pm");
            } else {
                System.out.println("The time now is: " + String.format("%02d", clockHours.getValue() % 12) + ":" + String.format("%02d", clockMinutes.getValue()) + "pm");
            }
        }
        /*
         if (format24) {
         System.out.println("The alarm is set: " + String.format("%02d",alarmHours.getValue()) + ":" + String.format("%02d",alarmMinutes.getValue()));
         } else {
         if (alarmHours.getValue() == 0) {
         System.out.println("The alarm is set: " + "12" + ":" + String.format("%02d",alarmMinutes.getValue()) + "am");
         } else if (alarmHours.getValue() > 0 && alarmHours.getValue() < 12) {
         System.out.println("The alarm is set: " + String.format("%02d",alarmHours.getValue()) + ":" + String.format("%02d",alarmMinutes.getValue()) + "am");
         } else if (alarmHours.getValue() == 12) {
         System.out.println("The alarm is set: " + "12" + ":" + String.format("%02d",alarmMinutes.getValue()) + "pm");
         } else {
         System.out.println("The alarm is set: " + String.format("%02d",alarmHours.getValue() % 12) + ":" + String.format("%02d",alarmMinutes.getValue()) + "pm");
         }
         }
         */
    }

    public void howManyAlarms() {
        System.out.println("There are " + alarms.size() + " alarms");
    }

    public void createAlarm(int hours, int minutes) {
        alarms.add(new Alarm(hours, minutes));
    }

    public void removeAlarm(int index) {
        if (index >= 0 && index < alarms.size()) {
            alarms.remove(index);
        } else {
            System.out.println("removeAlarm(): Enter a valid alarm index! No alarms removed!");
        }
    }

    public void listAlarms() {
        int hours = clockHours.getValue();
        String AmPm = "";

        for (int i = 0; i < alarms.size(); i++) {
            hours = alarms.get(i).getHours();

            // if this is a 12-hour-clock, print hours accordingly
            if (getFormat24() && alarms.get(i).getHours() > 12) {
                hours = alarms.get(i).getHours() - 12;
                AmPm = "pm";
            } else if (!getFormat24()) {
                AmPm = "am";
            }

            System.out.printf("Alarm %d is set for %02d:%02d %s\n", i, hours, alarms.get(i).getHours(), AmPm);
        }
    }

}
