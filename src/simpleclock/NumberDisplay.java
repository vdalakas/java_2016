/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simpleclock;

/**
 *
 * @author vdalakas
 * 
 * H κλάσση NumberDisplay δέχεται 2 ακέραια ορίσματα.
 * Τιμή
 * Όριο
 * 
 * 
 */
public class NumberDisplay {
    private int value;
    private int limit;

    NumberDisplay() {
        value = 0;
        limit = 0;
    }

    public void setValue(int newvalue) {
        value = newvalue;
    }

    public int getValue() {
        return value;
    }

    public void setLimit(int newlimit) {
        limit = newlimit;
    }

    public int getLimit() {
        return limit;
    }

    public void advanceValue(int value){
	this.value = (this.value + value) %limit;	
    }

    public int advanceValueWithOverflowCount(int value){
        value = this.value + value;
	int overflow = value / limit;
        return overflow;
    }
    
}
