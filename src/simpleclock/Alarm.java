package simpleclock;

import java.util.ArrayList;

public class Alarm {

    private NumberDisplay hours = new NumberDisplay();
    private NumberDisplay minutes = new NumberDisplay();

    Alarm() {
        hours.setValue(0);
        minutes.setValue(0);
        hours.setLimit(24);
        minutes.setLimit(60);
    }

    Alarm(int h, int m) {
        hours.setValue(h);
        minutes.setValue(m);
        hours.setLimit(24);
        minutes.setLimit(60);
    }

    public void setHours(int h) {
        if (h >= 0 && h <= hours.getLimit()) {
            hours.setValue(h);
        } else {
            System.out.println("Seriously man! What are you doing? Time has not been changed!" + h);
        }
    }

    public int getHours() {
        return hours.getValue();
    }

    public void setMinutes(int m) {
        if (m >= 0 && m <= 60) {
            minutes.setValue(m);
        } else {
            System.out.println("Seriously man! What are you doing? Time has not been changed!");
        }
    }

    public int getMinutes() {
        return minutes.getValue();
    }

    public void setFormat(int format) {
        hours.setLimit(format);
    }

}
